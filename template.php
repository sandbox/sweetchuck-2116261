<?php

/**
 * @file
 * Template overrides and (pre-)process and alter hooks for the Try DRY theme.
 */

/**
 * Implements hook_css_alter().
 */
function trydry_css_alter(&$css) {
  $assets = _trydry_assets();
  foreach ($assets['replace']['css'] as $media => $replacements) {
    _trydry_assets_alter($css, $replacements, $assets['meta']['css'], $media);
  }
}

/**
 * Implements hook_js_alter().
 */
function trydry_js_alter(&$js) {
  $assets = _trydry_assets();
  _trydry_assets_alter($js, $assets['replace']['js'], $assets['meta']['js']);
}

/**
 * Helper function for trydry_(css|js)_alter().
 *
 * CSS and JS replacements and overrides.
 *
 * @param array $candidates
 *   Array of CSS or JS to alter.
 * @param array $replacements
 *   Array of source and destination pairs.
 * @param array $meta
 *   Asset meta from the .info file.
 * @param string $media
 *   Only relevant for CSS.
 */
function _trydry_assets_alter(&$candidates, $replacements, $meta, $media = NULL) {
  $current_contexts = array_flip(_trydry_assets_contexts());
  foreach (array_intersect_key($replacements, $candidates) as $src_full => $contexts) {
    // Filter the contexts.
    $contexts = array_intersect_key($contexts, $current_contexts);

    $remove_original = TRUE;
    foreach ($contexts as $destinations) {
      foreach ($destinations as $dst_full) {
        if ($src_full === $dst_full) {
          $remove_original = FALSE;
          continue;
        }

        // Destination is already in the $assets,
        // ot the CSS media filter is set and it is not match.
        if (array_key_exists($dst_full, $candidates)
          || ($media && $candidates[$src_full]['media'] != $media)
        ) {
          continue;
        }

        $candidates[$dst_full] = $candidates[$src_full];
        $candidates[$dst_full]['data'] = $dst_full;
        $candidates[$dst_full]['weight'] += 0.001;
        if (isset($meta[$dst_full])) {
          $candidates[$dst_full] += $meta[$dst_full];
        }
      }
    }

    if ($remove_original) {
      unset($candidates[$src_full]);
    }
  }
}

/**
 * Get the full assets info from the .info file.
 *
 * @todo hook_system_info_alter().
 *
 * @param string $phase
 *   raw|processed
 * @param null|string $theme_id
 *   Theme machine name. The $theme_key name is reserved for the global
 *   variable.
 *
 * @return array
 *   The structure is depend on the $type and $phase arguments.
 *   Complete assets info. The result array contains the raw and the processed
 *   structures for "meta" and "replace" for JS and CSS.
 *   raw.meta.css
 *   raw.replace.css
 *   processed.meta.css
 *   processed.replace.css
 *
 *   And same structure for JS.
 */
function _trydry_assets($phase = 'processed', $theme_id = NULL) {
  if (!$theme_id) {
    $theme_id = $GLOBALS['theme_key'];
  }

  $cid = "trydry:assets:$theme_id";

  $assets =& drupal_static($cid, array());

  if (!isset($assets[$theme_id])) {
    $cache = cache_get($cid);
    if ($cache) {
      $assets[$theme_id] = $cache->data;
    }
  }

  // The system_get_info() does not provide information about the base themes.
  // And we need info about the parent themes.
  $themes = list_themes();
  if (!isset($assets[$theme_id])) {
    // Initialize the structure of the return variable.
    $assets[$theme_id] = array(
      'raw' => array(
        'meta' => array(
          'css' => array(),
          'js' => array(),
        ),
        'replace' => array(
          'css' => array(),
          'js' => array(),
        ),
      ),
      'processed' => array(
        'meta' => array(
          'css' => array(),
          'js' => array(),
        ),
        'replace' => array(
          'css' => array(),
          'js' => array(),
        ),
      ),
    );

    // @todo Throw error when the $theme_id is not exists.
    if (array_key_exists($theme_id, $themes)) {
      $theme = $themes[$theme_id];

      // Set the parent themes, and add the current (top level) theme to the end
      // of the list to treat easiest the hierarchy.
      // The top level theme is the strongest.
      $base_themes = array();
      if (isset($theme->base_themes)) {
        $base_themes = array_keys($theme->base_themes);
      }
      $base_themes[] = $theme_id;

      // Hierarchical array merge deep.
      // Loop over the fusible properties.
      foreach (array('meta', 'replace') as $property) {
        foreach ($base_themes as $base_theme_name) {
          if (isset($themes[$base_theme_name]->info["trydry_assets_{$property}"])) {
            $assets[$theme_id]['raw'][$property] = drupal_array_merge_deep(
              $assets[$theme_id]['raw'][$property],
              $themes[$base_theme_name]->info["trydry_assets_{$property}"]
            );
          }
        }
      }

      // Process the raw info array.
      // The CSS and JS "info" array can be processed together, because they
      // have same structure.
      _trydry_assets_process_meta($assets[$theme_id]);

      // The CSS "replace" array is one level deeper than it is JS counterpart,
      // because of the "media" level.
      foreach ($assets[$theme_id]['raw']['replace']['css'] as $media => $asset_list) {
        $assets[$theme_id]['processed']['replace']['css'][$media] = _trydry_assets_process_replace(
          'css',
          $asset_list,
          $base_themes
        );
      }

      // Process the raw replace array of JS.
      $assets[$theme_id]['processed']['replace']['js'] = _trydry_assets_process_replace(
        'js',
        $assets[$theme_id]['raw']['replace']['js'],
        $base_themes
      );
    }

    cache_set($cid, $assets[$theme_id]);
  }

  return $phase ? $assets[$theme_id][$phase] : $assets[$theme_id];
}

/**
 * Process the raw meta information in the .info file.
 *
 * @param array $assets
 *   Raw information array.
 */
function _trydry_assets_process_meta(&$assets) {
  $pattern = _trydry_assets_pattern_source();
  foreach ($assets['raw']['meta'] as $type => $asset_list) {
    foreach ($asset_list as $src_key => $meta) {
      $src = NULL;
      if (!preg_match($pattern, $src_key, $src)) {
        continue;
      }

      $src_path = _trydry_assets_owner_path($src['type'], $src['name']);
      if (!$src_path || _trydry_assets_check_incompatibility($src)) {
        continue;
      }

      $meta['data'] = "$src_path/{$src['relative']}";
      $meta['type'] = 'file';
      $assets['processed']['meta'][$type][$meta['data']] = $meta;
    }
  }
}

/**
 * Build a key from the asset properties.
 *
 * This kind of key are in use in the .info file.
 *
 * @param string[] $asset
 *   - type
 *   - name
 *   - relative
 *   - version: optional
 *   - context: optional
 *
 * @return string
 *   Composed key which is used in the .info file.
 */
function _trydry_assets_key($asset) {
  $key = $asset['type'] . ':' . $asset['name'] . ':' . $asset['relative'];
  if (isset($asset['version'])) {
    $key .= ':' . $asset['version'];
  }
  elseif (isset($asset['context'])) {
    $key .= ':' . $asset['context'];
  }

  return $key;
}

/**
 * Regular expression pattern for source asset key.
 *
 * @return string
 *   Regular expression pattern.
 */
function _trydry_assets_pattern_source() {
  return '/^(?P<type>theme|theme_engine|module|profile|library):(?P<name>[^:]+):(?P<relative>[^:]+)(:(?P<version>.+)){0,1}$/';
}

/**
 * Regular expression pattern for destination asset key.
 *
 * @return string
 *   Regular expression pattern.
 */
function _trydry_assets_pattern_destination() {
  return '/^(?P<type>theme|theme_engine|module|profile|library):(?P<name>[^:]+):(?P<relative>[^:]+)(:(?P<context>base|theme|skin|admin|demo)){0,1}$/';
}

/**
 * Check the version of the source asset is available in the current system.
 *
 * @todo Support for libraries.
 *
 * @see omega_check_incompatibility()
 *
 * @param array $src
 *   Exploded source key from the .info file.
 *
 * @return bool
 *   NULL if compatible, otherwise the original dependency version string that
 *   caused the incompatibility.
 */
function _trydry_assets_check_incompatibility($src) {
  if (empty($src['version'])) {
    return NULL;
  }

  $extension_info = system_get_info($src['type'], $src['name']);
  return omega_check_incompatibility(
    drupal_parse_dependency("{$src['name']} ({$src['version']})"),
    $extension_info['version']
  );
}

/**
 * Process the raw source destination pairs.
 *
 * @param string $type
 *   css|js
 * @param array $asset_list
 *   Pairs.
 * @param array $base_themes
 *   Theme parents. Numeric indexed array.
 *
 * @return array
 *   Processed pairs.
 */
function _trydry_assets_process_replace($type, $asset_list, $base_themes) {
  $pattern_source = _trydry_assets_pattern_source();
  $pattern_destination = _trydry_assets_pattern_destination();
  $return = array();
  foreach ($asset_list as $src_key => $destinations) {
    $src = NULL;
    if (!preg_match($pattern_source, $src_key, $src)) {
      continue;
    }

    $src_path = _trydry_assets_owner_path($src['type'], $src['name']);
    if (!$src_path || _trydry_assets_check_incompatibility($src)) {
      continue;
    }

    $src_full = "$src_path/{$src['relative']}";
    foreach (array_keys($destinations, TRUE) as $dst_key) {
      $dst = NULL;
      if (!preg_match($pattern_destination, $dst_key, $dst)) {
        continue;
      }

      $dst += array('context' => 'base');
      $dst_path = _trydry_assets_owner_path($dst['type'], $dst['name']);
      if (!$dst_path) {
        continue;
      }

      $return[$src_full][$dst['context']][] = "$dst_path/{$dst['relative']}";
    }
  }

  $overrides = _trydry_assets_overrides($type, $base_themes);
  foreach ($overrides as $src_full => $dst_full) {
    if (!isset($return[$src_full])) {
      $return[$src_full]['base'][] = $dst_full;
    }
  }

  return $return;
}

/**
 * Get the path pairs of overrides.
 *
 * @param string $type
 *   css|js
 * @param array $base_themes
 *   Array of names of parent themes.
 *
 * @return array
 *   Replace pairs.
 *   Key is the "from", value is the "to".
 */
function _trydry_assets_overrides($type, $base_themes) {
  $overrides =& drupal_static(__FUNCTION__, NULL);
  $theme_id = end($base_themes);
  $cid = "trydry:assets_overrides:$theme_id";

  if (!isset($overrides[$theme_id])) {
    $cache = cache_get($cid);
    if ($cache) {
      $overrides[$theme_id] = $cache->data;
    }
  }

  if (!isset($overrides[$theme_id])) {
    $relatives = _trydry_assets_overrides_relatives($type, $base_themes);
    $overrides[$theme_id] = array(
      'css' => array(),
      'js' => array(),
    );

    foreach (array_filter($relatives, 'is_array') as $absolutes) {
      $value = array_pop($absolutes);
      $overrides[$theme_id][$type] += array_fill_keys($absolutes, $value);
    }

    cache_set($cid, $overrides[$theme_id]);
  }

  return $overrides[$theme_id][$type];
}

/**
 * Group asset overrides by relative path.
 *
 * @param string $type
 *   Asset type CSS or JS.
 *   allowed values: css|js.
 * @param array $base_themes
 *   Array of machine names of base themes.
 *
 * @return array
 *   Key is the relative path, the value is an absolute path or an array of
 *   absolute paths. The last item in the array is the strongest.
 */
function _trydry_assets_overrides_relatives($type, $base_themes) {
  $theme_id = array_pop($base_themes);
  if (!$theme_id) {
    return array();
  }

  $relatives =& drupal_static(__FUNCTION__, array());
  $cid = "trydry:assets_overrides_relatives:$theme_id";

  if (!isset($relatives[$theme_id])) {
    $cache = cache_get($cid);
    if ($cache) {
      $relatives[$theme_id] = $cache->data;
    }
  }

  if (!isset($relatives[$theme_id])) {
    $relatives[$theme_id] = array(
      'css' => array(),
      'js' => array(),
    );

    foreach (array('css', 'js') as $current_type) {
      $relatives[$theme_id][$current_type] = array_merge_recursive(
        _trydry_assets_overrides_relatives($current_type, $base_themes),
        _trydry_assets_overrides_scan($current_type, $theme_id)
      );
    }

    cache_set($cid, $relatives[$theme_id]);
  }

  return $relatives[$theme_id][$type];
}

/**
 * Scan the assets files.
 *
 * @param string $type
 *   Allowed values: css|js.
 *
 * @param string $theme_id
 *   Machine name.
 *
 * @return array
 *   Key value pairs. Key is the relative path, the value is the absolute path.
 */
function _trydry_assets_overrides_scan($type, $theme_id) {
  $path = drupal_get_path('theme', $theme_id);
  $path_length = drupal_strlen("$path/");
  $files = file_scan_directory("$path/$type", "/\.{$type}$/");
  $return = array();
  foreach ($files as $file) {
    $relative = drupal_substr($file->uri, $path_length);
    $return[$relative] = $file->uri;
  }

  return $return;
}

/**
 * Get the path to the Drupal plugin.
 *
 * @param string $type
 *   Type of the plugin (theme, module, profile).
 * @param string $name
 *   Name of the plugin.
 *
 * @return bool|null|string
 *   Path to plugin.
 */
function _trydry_assets_owner_path($type, $name) {
  $path = NULL;
  switch ($type) {
    case 'core':
      $path = 'misc';
      break;

    case 'profile':
    case 'module':
    case 'theme':
      $path = drupal_get_path($type, $name);
      break;

    case 'library':
      if (module_exists('libraries')) {
        $path = libraries_get_path($name);
      }
      break;

  }

  return $path;
}

/**
 * Detect the current contexts.
 *
 * @return string[]
 *   CSS and JS context identifiers.
 *   Possible values:
 *   - base
 *   - theme
 *   - admin
 *   - demo
 */
function _trydry_assets_contexts() {
  $return = array(
    'base',
    'theme',
  );

  $path = current_path();
  if (path_is_admin($path)) {
    $return[] = 'admin';
  }

  // Add a special case for the block demo page.
  if (strpos($path, 'admin/structure/block/demo') === 0) {
    $return[] = 'demo';
  }

  return $return;
}
