/**
* @file
* Documentation missing.
*/

(function ($, Drupal) {
  'use strict';

  Drupal.admin = Drupal.admin || {};
  Drupal.admin.behaviors = Drupal.admin.behaviors || {};

  Drupal.admin.behaviors.activeTrail = function (context, settings, $adminMenu) {
    var
      currentPath = window.location.pathname,
      $first = $('a[href="' + currentPath + '"]', $adminMenu).first();

    $('a.active:not([href="' + currentPath + '"])', $adminMenu).each(function () {
      $(this)
        .removeClass('active')
        .parents()
        .removeClass('active-trail active-parent');
    });

    if ($first.length) {
      $first
        .addClass('active')
        .parents('ul, li')
        .addClass('active-trail')
        .slice(0, 1)
        .addClass('active-parent');
    }
  };

//  delete Drupal.admin.behaviors.hover;

}(jQuery, Drupal));
